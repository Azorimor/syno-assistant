package handlers

import (
	"github.com/gofiber/fiber/v2"
)

func InfoHandler(c *fiber.Ctx) error {
	return c.SendString("Handler Info")
}