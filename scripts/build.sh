#!/bin/bash

cd cmd/synass/

# Build für Windows (amd64)
GOOS=windows GOARCH=amd64 go build -o ../../build/synass_windows_amd64.exe .

# Build für Windows (arm64)
GOOS=windows GOARCH=arm64 go build -o ../../build/synass_windows_arm64.exe .

# Build für Linux (amd64)
GOOS=linux GOARCH=amd64 go build -o ../../build/synass_linux_amd64 .

# Build für Linux (arm64)
GOOS=linux GOARCH=arm64 go build -o ../../build/synass_linux_arm64 .

# Build für macOS (amd64)
GOOS=darwin GOARCH=amd64 go build -o ../../build/synass_mac_amd64 .

# Build für macOS (arm64)
GOOS=darwin GOARCH=arm64 go build -o ../../build/synass_mac_arm64 .