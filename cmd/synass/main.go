package main

import (
	"codeberg.org/azorimor/syno-assistant/internal/handlers"

	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()

	app.Get("/info", handlers.InfoHandler)

	err := app.Listen(":4004")
	if err != nil {
		return
	}
}