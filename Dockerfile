# Use a multi-architecture base image
FROM --platform=${TARGETPLATFORM:-linux/amd64} golang:latest

WORKDIR /app

COPY . .

RUN go build -o main .

EXPOSE 4004

CMD ["./main"]