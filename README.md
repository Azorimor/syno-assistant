# Syno Assistant (synass)

Integrate an OpenAI assistant into Synology Chat and provide a web interface for the assistant

## TODO
- [ ] Create simple CI config
- [ ] Create Synology Chat client for recieving bot messages and sending bot messages
- [ ] Verify messages from synology
- [ ] Integrate OpenAI Assistant to be used for chat completions
- [ ] Create web integration (with embedding for custom sites)
- [ ] Add public rate limits
- [ ] provide simple documentation for how to use synass.